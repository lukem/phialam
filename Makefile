# Makefile

MODULES=phialam demo

DLIST:=missing-function-docstring,missing-module-docstring,duplicate-code
DLIST:=$(DLIST),missing-class-docstring,too-few-public-methods
DLIST:=$(DLIST),too-many-arguments,too-many-locals,too-many-instance-attributes
DLIST:=$(DLIST),too-many-branches,too-many-statements,broad-except
#DLIST:=$(DLIST),too-many-boolean-expressions,too-many-return-statements

.PHONY: lint
lint: blued
	mypy $(MODULES)
	-flake8 $(MODULES)
	-pylint --disable=$(DLIST) $(MODULES)

.PHONY: blued
blued:
	blue $(MODULES)
