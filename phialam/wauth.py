#!/usr/bin/env python3
# wauth.py -*-python-*-


import time

import flask
import flask_login

# import werkzeug.security

import phialam.db
import phialam.model

# from phialam.log import DEBUG

auth = flask.Blueprint('auth', __name__)


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if flask_login.current_user.is_authenticated:
        flask.flash(
            f'User "{flask_login.current_user.email}" already authenticated'
        )
        return flask.redirect(flask.url_for('root.index'))

    if flask.request.method == 'POST':
        email = flask.request.form.get('email')
        password = flask.request.form.get('password')
        remember = flask.request.form.get('remember', False)

        error = None
        if not email:
            error = 'Email is required'
        elif not password:
            error = 'Password is required'
        else:
            user = phialam.model.User.query.filter_by(email=email).first()
            if not user or not user.check_password(password):
                error = 'Please check login details and try again'

        if error is None:
            user.login_count += 1
            user.last_login = int(time.time())
            flask.g.db_session.commit()
            flask_login.login_user(user, remember=remember)
            return flask.redirect(flask.url_for('root.index'))

        flask.flash(error)
    return flask.render_template('login.html')

@auth.route('/logout')
def logout():
    flask_login.logout_user()
    return flask.redirect(flask.url_for('root.index'))

@auth.route('/register', methods=['GET', 'POST'])
def register():
    if flask.request.method == 'POST':
        error = None
        INFO(repr(flask.request.form))
        email = flask.request.form.get('email', None)
        name = flask.request.form.get('name', None)
        passwd = flask.request.form.get('password', None)
        passwd2 = flask.request.form.get('password2', None)
        # data = flask.request.form.get('data', None)

        if not email:
            error = 'Email is required'
        elif not name:
            error = 'Name is required'
        elif not passwd:
            error = 'Password is required'
        elif passwd != passwd2:
            error = 'Passwords do not match'
        else:
            result = zxcvbn.zxcvbn(passwd, user_inputs=[email, name])
            if result['score'] <= 2:
                error = ''
                if len(passwd) < 8:
                    error = 'Please use at least 8 characters. '
                error += result['feedback']['warning']
                for suggestion in result['feedback']['suggestions']:
                    error += f'<br>{suggestion}'

        if error is None:
            user = phialam.model.User.query.filter_by(email=email).first()
            if user:
                error = flask.Markup('''
{email} is already registered. Maybe you want the
<a href="{flask.url_for('auth.login')}">login page</a>''')

        if error is None:
            current_time = int(time.time())
            user = phialam.model.User(
                email=email,
                name=name,
                password=werkzeug.security.generate_password_hash(passwd),
                registered=current_time,
                confirmed=0,
                login_count=0,
                last_login=0,
                forgot_count=0,
                last_forgot=0,
                last_change=current_time,
                access='',
                force_password_change=0)
            # pylint: disable=no-member
            flask.g.db_session.add(user)
            flask.g.db_session.commit()
            send_confirmation_email(email, name)
            flask.flash(f'Confirmation email has been sent to {email}')
            return flask.redirect(flask.url_for('auth.login'))

        flask.flash(error)

    challenge = get_challenge()
    INFO(f'challenge={challenge}')
    flask.flash(flask.Markup('''
    <div class="content"><ul>
    <li>Passwords should be at least 8 character long.</li>
    <li>Spaces are ok.</li>
    <li>Use something memorable.</li>
    <li>Weird punctuation is not required.</li>
    </ul></div>'''),
                'info')
    return flask.render_template(
        'register.html',
        sitekey=flask.current_app.config['HCAPTCHA_SITEKEY'])

@auth.route('/forgot', methods=['GET', 'POST'])
def forgot():
    if flask.request.method == 'POST':
        error = None
        print(flask.request.form)
        email = flask.request.form.get('email', None)
        hcaptcharesponse = flask.request.form.get('h-captcha-response')

        if not email:
            error = 'Email is required'
        elif not hcaptcha_ok(hcaptcharesponse):
            error = 'Please verify you are a human'
        else:
            user = phialam.model.User.query.filter_by(email=email).first()
            if not user:
                error = 'Please recheck email address'

        if error is None:
            # Use the class here to avoid a race condition.
            user.forgot_count = phialam.model.User.forgot_count + 1
            user.last_forgot = int(time.time())
            # pylint: disable=no-member
            flask.g.db_session.commit()
            send_forgot_email(email, user.name)
            flask.flash(f'Password reset email has been sent to {email}',
                        'info')
            return flask.redirect(flask.url_for('auth.login'))

        flask.flash(error)

    return flask.render_template(
        'forgot.html',
        sitekey=flask.current_app.config['HCAPTCHA_SITEKEY'])


@auth.route('/password', methods=['GET', 'POST'])
def password():
    '''Change the users password. If force is True, then the request came via
    email and we don't ask for the user's password for confirmation. Otherwise,
    we confirm the old password and return to the /preferences page.'''

    authenticated = flask_login.current_user.is_authenticated
    force = authenticated and flask_login.current_user.force_password_change
    if not force and not authenticated:
        flask.flash('Must login to change password')
        return flask.redirect(flask.url_for('auth.login'))

    if flask.request.method == 'POST':
        error = None
        oldpassword = flask.request.form.get('oldpassword', '')
        newpassword = flask.request.form.get('newpassword', '')
        newpassword2 = flask.request.form.get('newpassword2', '')

        if oldpassword == '' and \
           newpassword == '' and \
           newpassword2 == '':
            return flask.render_template('password.html')

        if newpassword == '':
            if not force and oldpassword == '':
                error = 'Please enter old and new passwords'
            else:
                error = 'Please enter new password'
        elif not force and oldpassword == '':
            error = 'Please enter old password'

        if not force and newpassword != newpassword2:
            error = 'New passwords do not match'

        if oldpassword != '' and \
           not force and \
           not werkzeug.security.check_password_hash(
                flask_login.current_user.password, oldpassword):
            error = 'Old password incorrect'

        if error is None and newpassword != '':
            # pylint: disable=assigning-non-slot,no-member
            flask_login.current_user.force_password_change = False
            flask_login.current_user.password = \
                werkzeug.security.generate_password_hash(newpassword)
            flask_login.current_user.last_change = int(time.time())
            # pylint: disable=no-member
            flask.g.db_session.commit()
            if force:
                flask_login.logout_user()
            flask.flash('Password updated')

            if force:
                return flask.redirect(flask.url_for('auth.login'))
            return flask.redirect(flask.url_for('root.preferences'))

        if error is not None:
            flask.flash(error)
    return flask.render_template('password.html')

@auth.route('/confirm/<token>')
def confirm(token):
    # pylint: disable=no-member
    email, purpose = confirm_token(token)
    user = phialam.model.User.query.filter_by(email=email).first()
    if email is None or user is None:
        flask.flash('The confirmation link is invalid or has expired.')
        return flask.redirect(flask.url_for('auth.login'))

    if purpose == 'confirm':
        if user.confirmed:
            flask.flash('Account already confirmed.')
            return flask.redirect(flask.url_for('auth.login'))

        user.confirmed = int(time.time())
        flask.g.db_session.commit()
        flask.flash('You have confirmed your account.')
    elif purpose == 'forgot':
        # TODO: Login user to get state, but flag state so that nothing works.
        flask_login.login_user(user)
        user.force_password_change = True
        flask.g.db_session.commit()
        INFO('CALLING PASSWORD()')
        return password()
    return flask.redirect(flask.url_for('auth.login'))
