#!/usr/bin/env python3
# wmain.py -*-python-*-

import flask
import flask_bootstrap
import flask_login
import jinja2

# import flask_mail
# import flask_sqlalchemy

import phialam.model
import phialam.wauth
import phialam.wroot


app = flask.Flask(__name__, static_url_path='/static')


def create_app(config, db, debug=False, debughtml=False):
    app.debug = debug
    for key, value in config.section('flask').items():
        app.config[key.upper()] = value
    for key, value in config.section().items():
        if key.startswith('hcaptcha') or key in {'title', 'favicon'}:
            app.config[key.upper()] = value
    app.config['DEBUGHTML'] = debughtml

    # The login manager
    login_manager = flask_login.LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    # Bootstrap
    Bootstrap = flask_bootstrap.Bootstrap5(app)

    # Have jinja2 use more paths.
    jinja_loader = jinja2.ChoiceLoader([
        app.jinja_loader,
        jinja2.FileSystemLoader(['templates', 'more_templates']),
        ])
    app.jinja_loader = jinja_loader

    #app.extensions['phialam'] = {'db': db, 'login_manager': login_manager}

    @login_manager.user_loader
    def load_user(user_id):
        return phialam.model.User.query.get(int(user_id))

    # Support the cookie consent box
    @app.context_processor
    def inject_template_scope():
        injections = {}

        def cookies_check():
            value = flask.request.cookies.get('cookie-consent')
            return value == 'true'

        injections.update(cookies_check=cookies_check)
        return injections

    @app.before_request
    def before_request():
        flask.g.db = db
        flask.g.db_session = db.session
        flask.g.db_session()

    @app.after_request
    def after_request(response):
        flask.g.db_session.remove()
        return response

    #    # We don't import these at the top because that causes a circular
    #    # dependency.
    #    import indago.wauth
    app.register_blueprint(phialam.wauth.auth)
    #    import indago.wroot
    app.register_blueprint(phialam.wroot.root)
    #    import indago.wsearch
    #    app.register_blueprint(indago.wsearch.search)

    return app
