#!/usr/bin/env python3
# db.py -*-python-*-

import time
from typing import Any, Dict

# pylint: disable=no-name-in-module
from psycopg2.errors import DuplicateTable, UndefinedTable
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from sqlalchemy import create_engine, text
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.engine.url import URL
from sqlalchemy.exc import (
    ProgrammingError,
    OperationalError,
    ResourceClosedError,
)

# from sqlalchemy.schema import DropTable
# from sqlalchemy.ext.compiler import compiles

import phialam.config
from phialam.log import DEBUG, ERROR, FATAL, FATAL_DECODE
import phialam.model


class DB:
    def __init__(
        self, config: phialam.config.Config, driver: str = 'postgresql'
    ):
        self.config = config
        self.driver = driver

        dbconfig = self._get_dbconfig()
        url = URL.create(**dbconfig)
        DEBUG(f'{url=}')

        self.engine = create_engine(url)
        self.session = scoped_session(
            sessionmaker(autocommit=False, autoflush=False, bind=self.engine)
        )
        phialam.model.base.query = self.session.query_property()

    def _get_dbconfig(self, excluded_keys=None) -> Dict[str, Any]:
        if excluded_keys is None:
            excluded_keys = []

        valid_keys = []
        for key in [
            'drivername',
            'username',
            'password',
            'host',
            'port',
            'database',
            'query',
        ]:
            if key in excluded_keys:
                continue
            valid_keys.append(key)

        dbconfig: Dict[str, Any] = {'drivername': self.driver}
        for key, value in self.config.section(self.driver).items():
            if key in valid_keys:
                dbconfig[key] = value
        return dbconfig

    def _select(
        self, command, args=None, zeroth=True, engine=None, isolation=False
    ) -> list[str]:
        if engine is None:
            engine = self.engine

        results = []
        try:
            with engine.connect() as conn:
                if isolation:
                    conn.connection.set_isolation_level(
                        ISOLATION_LEVEL_AUTOCOMMIT
                    )
                try:
                    cur = conn.execute(command, args)
                    for result in cur:
                        results.append(result[0] if zeroth else result)
                except ResourceClosedError:
                    # This result does not return rows.
                    pass
        except OperationalError as exception:
            error = 'Cannot connect'
            if 'does not exist' in repr(exception):
                FATAL(f'{error} (use --init-db to create the database)')
            FATAL_DECODE(error)
        return results

    def init_db(self) -> None:
        database = self.config.section(self.driver).get('database', None)
        if database is None:
            # This can't happen because of the checking in phialam.config
            FATAL('Cannot create database: no name in config file')
        dbconfig = self._get_dbconfig(excluded_keys=['database'])
        url = URL.create(**dbconfig)
        engine = create_engine(url)
        self._select(
            text(f'drop database if exists {database}'),
            engine=engine,
            isolation=True,
        )
        self._select(
            text(f'create database {database}'), engine=engine, isolation=True
        )

    def version(self) -> str:
        if self.driver == 'sqlite':
            versions = self._select(text('select sqlite_version()'))
        else:
            versions = self._select(text('select version()'))
        return versions[0]

    def show_db(self) -> list[str]:
        output = []
        output.append('Database')
        output.append(f'  Driver: {self.driver}')
        output.append(f'  Version: {self.version()}')
        return output

    def add_user(
        self,
        email: str,
        name: str,
        password: str,
        access: str,
        force_password_change: bool = False,
    ) -> None:
        current_time = int(time.time())
        user = phialam.model.User(
            email=email,
            name=name,
            password=password,
            registered=current_time,
            confirmed=current_time,
            login_count=0,
            last_login=0,
            forgot_count=0,
            last_forgot=0,
            last_change=0,
            access=access,
            force_password_change=0 if not force_password_change else 1,
        )
        self.session.add(user)
        self.session.commit()

    def init_user(self):
        try:
            phialam.model.base.metadata.tables['user'].create(self.engine)
        except ProgrammingError as exception:
            if isinstance(exception.orig, DuplicateTable):
                ERROR('The "user" table already exists')
            else:
                FATAL_DECODE('Could not create "user" table')
        except OperationalError as exception:
            error = 'Cannot create "user" table'
            if 'does not exist' in repr(exception):
                FATAL(f'{error} (use --init-db to create the database)')
            FATAL_DECODE(error)

        self.add_user(
            'admin',
            'admin',
            self.config.section()['admin_hash'],
            'admin',
            force_password_change=False,
        )

    def show_user(self) -> list[str]:
        output = [
            f'{"id":>5s} {"email":<20s} {"name":<20s} {"registered":<24s}'
            f' {"confirmed":<24s} {"count":>5s} {"last_login":<24s}'
            f' {"count":>5s} {"last_forgot":<24s} {"last_change":<24s} access'
        ]
        try:
            for user in self.session.query(phialam.model.User).all():
                rt = (
                    time.ctime(user.registered) if user.registered else 'never'
                )
                ct = time.ctime(user.confirmed) if user.confirmed else 'never'
                llt = (
                    time.ctime(user.last_login) if user.last_login else 'never'
                )
                lft = (
                    time.ctime(user.last_forgot)
                    if user.last_forgot
                    else 'never'
                )
                lct = (
                    time.ctime(user.last_change)
                    if user.last_change
                    else 'never'
                )
                output.append(
                    f'{user.id:5d}'
                    f' {user.email if user.email is not None else "":<20s}'
                    f' {user.name if user.name is not None else "":<20s}'
                    f' {rt:<24s}'
                    f' {ct:<24s}'
                    f' {user.login_count:5d}'
                    f' {llt:<24s}'
                    f' {user.forgot_count:5d}'
                    f' {lft:<24s}'
                    f' {lct:<24s}'
                    f' {user.access if user.access is not None else ""}'
                )
        except ProgrammingError as exception:
            if isinstance(exception.orig, UndefinedTable):
                ERROR(
                    'The "user" table does not exist'
                    ' (use --init-user to create the "user" table)'
                )
                return []
            FATAL_DECODE('Could not access "user" table')
        return output

    def drop_user(self) -> None:
        try:
            phialam.model.base.metadata.tables['user'].drop(self.engine)
        except ProgrammingError as exception:
            if isinstance(exception.orig, UndefinedTable):
                ERROR('The "user" table does not exist')
            else:
                FATAL_DECODE('Could not drop "user" table')
        except OperationalError as exception:
            error = 'Cannot drop "user" table'
            if 'does not exist' in repr(exception):
                FATAL(f'{error} (use --init-db to create the database)')
            FATAL_DECODE(error)
