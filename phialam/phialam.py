#!/usr/bin/env python3
# main.py -*-python-*-

import argparse
import getpass
from typing import List, Optional

import waitress
import werkzeug.security

import phialam.config
import phialam.db
from phialam.log import LOG_SET_LEVEL, ERROR, FATAL
import phialam.wmain


class Phialam:
    def __init__(
        self,
        project_name: str,
        project_description: str,
        project_config: Optional[List[str]] = None,
    ) -> None:
        self.project_name = project_name
        self.project_description = project_description
        self.project_config: Optional[List[str]] = project_config
        self.parser: Optional[argparse.ArgumentParser] = None
        self.config: Optional[phialam.config.Config] = None
        self.db: Optional[phialam.db.DB] = None

    def get_parser(self) -> argparse.ArgumentParser:
        if self.parser:
            return self.parser

        self.parser = argparse.ArgumentParser(
            description='Phialam Demo',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )

        cgroup = self.parser.add_argument_group('Configuration')
        cgroup.add_argument(
            '--config', default=None, help='Configuration file'
        )
        cgroup.add_argument(
            '--show-config',
            action='store_true',
            default=False,
            help='Show configuration and exit',
        )
        cgroup.add_argument(
            '--hash',
            action='store_true',
            default=False,
            help='Generate a PBKDF2 hash (for use in config file)',
        )

        dgroup = self.parser.add_argument_group('Database')
        dgroup.add_argument(
            '--init-db',
            action='store_true',
            default=False,
            help='Drop and create the database (DESTRUCTIVE!)',
        )

        dgroup.add_argument(
            '--show-db',
            action='store_true',
            default=False,
            help='Show information about the database',
        )

        ugroup = self.parser.add_argument_group('User Database')
        ugroup.add_argument(
            '--init-user',
            action='store_true',
            default=False,
            help='Initialize user database table (DESTRUCTIVE!)',
        )
        ugroup.add_argument(
            '--drop-user',
            action='store_true',
            default=False,
            help='Drop the user database table (DESTRUCTIVE!)',
        )
        ugroup.add_argument(
            '--show-user',
            action='store_true',
            default=False,
            help='Show user database and exit',
        )

        wgroup = self.parser.add_argument_group('Web Server')
        wgroup.add_argument(
            '--server',
            action='store_true',
            default=False,
            help='Start production web server',
        )
        wgroup.add_argument(
            '--devserver',
            action='store_true',
            default=False,
            help='Start development web server',
        )
        wgroup.add_argument(
            '--host', default='127.0.0.1', help='Interface for web server'
        )
        wgroup.add_argument(
            '--port', type=int, default=8888, help='Port for web server'
        )

        dgroup = self.parser.add_argument_group('Debugging')
        dgroup.add_argument(
            '--debug',
            action='store_true',
            help='Provide debugging output',
        )
        dgroup.add_argument(
            '--nodebug', nargs='+', help='Modules for which debug is disables'
        )
        dgroup.add_argument(
            '--debughtml',
            action='store_true',
            default=False,
            help='Display raw database values in html output',
        )

        return self.parser

    # pylint: disable=too-many-return-statements
    def get_args(self) -> Optional[argparse.Namespace]:
        if not self.parser:
            return None
        args = self.parser.parse_args()

        if args.debug:
            LOG_SET_LEVEL('DEBUG')
            LOG_SET_LEVEL('INFO', module='sqlalchemy.engine')

        if args.nodebug:
            for module in args.nodebug:
                if module.startswith('sqlalchemy'):
                    LOG_SET_LEVEL('ERROR', module=module)
                else:
                    LOG_SET_LEVEL('INFO', module=module)

        if args.hash:
            pw1 = getpass.getpass('Enter password: ')
            pw2 = getpass.getpass('Re-enter password: ')
            if pw1 != pw2:
                ERROR('Passwords do not match')
            print(werkzeug.security.generate_password_hash(pw1))
            return None

        if args.config:
            if isinstance(args.config, str):
                self.project_config = [args.config]
            else:
                self.project_config = args.config
        self.config = phialam.config.Config(self.project_name)

        if args.show_config:
            self.config.show()
            return None

        self.db = phialam.db.DB(self.config)
        if not self.db:
            FATAL('Could not initialized connection with database')

        if args.init_db:
            self.db.init_db()
            return None
        if args.show_db:
            for line in self.db.show_db():
                print(line)
            return None

        if args.init_user:
            self.db.init_user()
            return None
        if args.show_user:
            for line in self.db.show_user():
                print(line)
            return None
        if args.drop_user:
            self.db.drop_user()
            return None

        if args.server or args.devserver:
            app = phialam.wmain.create_app(
                self.config,
                self.db,
                debug=args.debug if args.server else True,
                debughtml=args.debughtml,
            )
            if args.server:
                waitress.serve(
                    app,
                    host=args.host,
                    port=args.port,
                    url_scheme='https',
                    log_untrusted_proxy_headers=True,
                    trusted_proxy='localhost',
                    trusted_proxy_headers='x-forwarded-for'
                    + ' x-forwarded-host x-forwarded-port',
                )
            else:
                # Development server: reloads on file changes.
                app.run(
                    debug=args.debug,
                    host=args.host,
                    port=args.port,
                    ssl_context='adhoc',
                )

        return args
