#!/usr/bin/env python3
# config.py -*-python-*-

import configparser
import os
from typing import List, Optional
import uuid

from phialam.log import FATAL


class Config:
    def __init__(
        self,
        project_name: str,
        project_title: Optional[str] = None,
        favicon_path: Optional[str] = None,
        config_paths: Optional[List[str]] = None,
    ):
        self.project_name = project_name
        self.project_title = project_title or project_name.upper()
        self.favicon_path = favicon_path or f'{project_name}.ico'
        self.config_paths = config_paths or [
            f'~/.config/{project_name}',
            f'~/.{project_name}',
        ]

        self.paths = []
        for path in self.config_paths:
            self.paths.append(os.path.expanduser(path))

        self.config = configparser.ConfigParser()

        # These values must be specified in the config file so that they are
        # the same across restarts.
        secret = uuid.uuid4().hex
        salt = uuid.uuid4().hex

        # Load defaults.
        self.config.read_dict(
            {
                self.project_name: {
                    'title': self.project_title,
                    'favicon': self.favicon_path,
                },
                'flask': {
                    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
                    'TEMPLATES_AUTO_RELOAD': True,
                    'SECRET_KEY': secret,
                    'SECURITY_PASSWORD_SALT': salt,
                    'REMEMBER_COOKIE_SECURE': True,
                    'SESSION_COOKIE_HTTPONLY': True,
                    'REMEMBER_COOKIE_HTTPONLY': True,
                },
            }
        )
        self.config.read(self.paths)

        error = ''
        if (
            not self.config.has_section(self.project_name)
            or 'admin_hash' not in self.config[self.project_name]
        ):
            error += f"""
    * The [{self.project_name}] section must have an "admin_hash" key. The
      value of the key is a hash of the admin password, generated using
      --hash."""

        if (
            not self.config.has_section('postgresql')
            or 'host' not in self.config['postgresql']
            or 'database' not in self.config['postgresql']
            or 'user' not in self.config['postgresql']
            or 'password' not in self.config['postgresql']
        ):
            error += """

    * The [postgresql] section must have "host", "database", "user" and
      "password" keys."""

        if not self.config.has_section('flask') or (
            self.config['flask']['SECRET_KEY'] == secret
            or self.config['flask']['SECURITY_PASSWORD_SALT'] == salt
            or self.config['flask']['SECRET_KEY']
            == self.config['flask']['SECURITY_PASSWORD_SALT']
        ):
            error += """

    * The [flask] section must have "secret_key and "security_password_salt"
      keys. Otherwise, sessions will not work correctly across server
      restarts. Further, these values must be unique."""

        if error:
            FATAL(
                f'The configuration file (e.g., {self.config_paths[0]})'
                ' requires certain key=value pairs in certain sections:\n'
                + error
            )

    def show(self) -> None:
        print('config')
        for section in self.config.sections():
            print(f'  [{section}]')
            for key, value in self.config[section].items():
                print(f'    {key} = {value}')

    def section(
        self, section: Optional[str] = None
    ) -> configparser.SectionProxy:
        if section:
            return self.config[section]
        return self.config[self.project_name]
