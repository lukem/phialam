#!/usr/bin/env python3
# model.py -*-python-*-

import flask_login
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String

# from sqlalchemy import UnicodeText
# from sqlalchemy import ForeignKey
# from sqlalchemy.dialects.postgresql import ARRAY
# from sqlalchemy.orm import relationship, declarative_base
from sqlalchemy.orm import declarative_base
import werkzeug

# import phialam.db
# from phialam.log import FATAL

base = declarative_base()


class User(base, flask_login.UserMixin):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    email = Column(String(100), unique=True)
    name = Column(String(1000))
    password = Column(String(200))
    registered = Column(Integer)
    confirmed = Column(Integer)
    login_count = Column(Integer)
    last_login = Column(Integer)
    forgot_count = Column(Integer)
    last_forgot = Column(Integer)
    last_change = Column(Integer)
    access = Column(String(200))
    force_password_change = Column(Integer)

    #    def __init__(self, ident):
    #        self.id = ident

    def check_password(self, password):
        return werkzeug.security.check_password_hash(self.password, password)
