#!/usr/bin/env python3
# main.py -*-python-*-

import sys

import phialam.phialam


def main() -> None:
    ph = phialam.phialam.Phialam('demo', 'Phialam Demo')
    parser = ph.get_parser()
    parser.add_argument('--word', default=None, help='Word to be displayed')
    args = ph.get_args()
    if args and args.word:
        print(args.word)
    sys.exit(0)
