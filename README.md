= Phialam =

A "bowl" (i.e., phialam) for flask projects, providing a tiny framework for
login, etc.

== Thanks ==

Thanks to
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world
for some helpful examples.
